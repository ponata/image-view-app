'use strict';

module.exports =
    angular.module('expressly.image', [])
        .controller('imageController', require('./controllers/image.controller'));
