'use strict';

module.exports = /*@ngInject*/
    function imageController($scope, httpService, $routeParams) {

        $scope.imageId = $routeParams.id;

        httpService.getImageById($scope.imageId).then(
            function (data) {
                $scope.currentImage = data;
            },
            function (error) {
                console.log(error);
            }
        );
    };
