'use strict';

module.exports = /*@ngInject*/
    function mainController($scope, httpService, $location, $document) {
        $scope.allImages = [];
        
        angular.element($document).on('scroll', function($event) {
            var body = $event.target.scrollingElement;
            if (body.scrollHeight <= body.scrollTop + body.clientHeight) {
                var newBucket = httpService.getNewImageBucket($scope.allImages.length);
                $scope.allImages = $scope.allImages.concat(newBucket);
                $scope.$apply();
            }
        });

        httpService.getAllImages().then(
            function (data) {
                $scope.allImages = $scope.allImages.concat(data);
            },
            function (error) {
                console.log(error);
            }
        );
    };
