'use strict';

module.exports =
    angular.module('expressly.main', []).controller('mainController', require('./controllers/main.controller'));
