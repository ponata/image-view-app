'use strict';

module.exports =
    angular.module('expressly.album', [])
        .controller('albumController', require('./controllers/album.controller'));
