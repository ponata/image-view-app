'use strict';

module.exports = /*@ngInject*/
    function albumController($scope, httpService, $routeParams) {

        $scope.albumId = $routeParams.id;


        httpService.getAllImagesfromAlbum($scope.albumId).then(
            function (data) {
                $scope.allImagesfromAlbum = data;
            },
            function (error) {
                console.log(error);
            }
        );

    };
