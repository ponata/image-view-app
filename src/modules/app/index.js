'use strict';

module.exports =
    angular.module('expressly', [
        'ngRoute',
        'ngAnimate',
        'ngAria',
        'ngMaterial',
        'pascalprecht.translate',

        // html templates in $templateCache
        require('../../../tmp/templates').name,

        // common directives, filters, services
        require('../common').name,


        // modules
        require('./main').name,
        require('./album').name,
        require('./image').name
    ])
        .config(require('./routes.js'));
