'use strict';

module.exports = /*ngInject*/
    function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/main/templates/main.html',
                controller: 'mainController'
            })
            .when('/album/:id', {
                templateUrl: 'app/album/templates/album.html',
                controller: 'albumController'
            })
            .when('/images/:id', {
                templateUrl: 'app/image/templates/image.html',
                controller: 'imageController'
            })
            .otherwise({
                redirectTo: '/'
            });
            $locationProvider.html5Mode({
              enabled: true,
              requireBase: false
            });
    };
