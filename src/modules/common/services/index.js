'use strict';

module.exports =
    angular.module('expressly.common.services', [])
        .constant("apiUrl", "http://jsonplaceholder.typicode.com/")
        .factory('httpService', require('./http.service'));
