'use strict';

module.exports = /*@ngInject*/
    function httpService($http, apiUrl)  {
        var allImages;

        this.getAllImages = function() {
            return $http({
                method: 'GET',
                url: apiUrl + 'photos/',
            }).then(
                function (response) {
                    allImages = response.data;
                    return allImages.slice(0, 30);
                },
                function (error) {
                    console.log(error);
                }
            );
        };

        this.getAllImagesfromAlbum = function(albumId) {
            return $http({
                method: 'GET',
                url: apiUrl + 'album/'+ albumId + '/photos/',
            }).then(
                function (response) {
                    return response.data;
                },
                function (error) {
                    console.log(error);
                }
            );
        };

        this.getImageById = function(imageId) {
            return $http({
                method: 'GET',
                url: apiUrl + 'photos/'+ imageId,
            }).then(
                function (response) {
                    return response.data;
                },
                function (error) {
                    console.log(error);
                }
            );
        };

        this.getNewImageBucket = function (firstImageIndex) {
            return allImages.slice(firstImageIndex, firstImageIndex+30);
        };

        return this;
    };
